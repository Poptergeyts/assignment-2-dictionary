import subprocess

input_list = ["integer", "asd", "", "a" * 300, "short"]
output_list = ["size is 32 bits", "", "", "", "size is 16 bits"]
error_list = ["", "ERROR: Value is not found", "ERROR: Value is not found", "ERROR: Input string is too long", ""]

for i in range(len(input_list)):
	subproc = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = subproc.communicate(input=input_list[i].encode())
	out_message = stdout.decode().strip()
	error_message = stderr.decode().strip()
	
	if error_message == error_list[i] and out_message == output_list[i]:
		print("Test {} is successful".format(i + 1))
		if out_message == "":
			print(error_message)
		else:
			print("OUTPUT: {}".format(out_message))
	else:
		print("Test {} failed".format(i + 1))
		print("OUTPUT: {}".format(out_message))
		print(error_message)
