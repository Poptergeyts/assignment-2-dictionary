%ifndef COLON_INC
	%define COLON_INC
	%define last_entry 0
	%macro colon 2
		%ifstr %1
			%ifid %2
				%2:
					dq last_entry
					dq %1, 0
				%define last_entry %2
			%else
				%error "ERROR: Invalid label"
			%endif
		%else
			%error "ERROR: The key must be a string"
		%endif
	%endmacro
%endif
