%include "lib.inc"
%define LONG 8

section .text
global find_word

find_word:
	.loop:
		test rsi, rsi
		jz .map_ended
		add rsi, LONG
		push rsi
		call string_equals
		pop rsi
		test rax, rax
		jnz .found

		sub rsi, LONG
		test rsi, rsi
		jz .map_ended
		jmp .loop

	.found:
		mov rax, rsi
		ret
	
	.map_ended:
		xor rax, rax
		ret
