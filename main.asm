%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define LENGTH 256

section .data
long_str: db "ERROR: Input string is too long", 0
not_found: db "ERROR: Value is not found", 0

section .bss
input_string: resb 256

section .text
global _start

_start:
	mov rdi, input_string
	mov rsi, LENGTH
	mov rcx, long_str
	push rcx
	call read_string
	test rax, rax
	jz .print_error

	mov rdi, rax
	mov rsi, long_size
	mov rcx, not_found
	push rcx
	call find_word
	test rax, rax
	jz .print_error

	mov rdi, rax
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rax
	call print_string
	call print_newline

	xor rdi, rdi
	jmp exit

	.print_error:
		pop rcx
		call print_error
		mov rdi, 1
		jmp exit

	
