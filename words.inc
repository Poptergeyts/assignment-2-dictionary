%include "colon.inc"

section .data

colon "byte", byte_size
db "size is 8 bits", 0

colon "short", short_size
db "size is 16 bits", 0

colon "integer", int_size
db "size is 32 bits", 0

colon "long", long_size
db "size is 64 bits", 0
