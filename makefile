ASM = nasm
ASMFLAGS = -felf64

.PHONY: test clean run

.all: clean lib.o, dict.o, main.o, main

main: main.o dict.o lib.o
	ld -o $@ $^

main.o: main.asm dict.o lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

test: main
	python2.7 test.py

clean: 
	rm -rf *.o main

run: 
	main




